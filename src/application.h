/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/


#ifndef EBAPPLICATION_H
#define EBAPPLICATION_H

#include <QApplication>

class QSystemTrayIcon;
class QMenu;
class QAction;

namespace eb
{

class CameraThread;
class SetupDialog;

class Application : public QApplication
{
    Q_OBJECT

public:
    Application(int &argc, char **argv);
    ~Application();

private slots:
    void setup();

    void saveSetup(int threshold, int cameraIdx);

private:
    void createMenus();
    void createWidgets();
    void restoreSettings();

    QSystemTrayIcon *systemTray;
    QMenu *trayMenu;
    QAction *exitAct;

    CameraThread *camThread;
    SetupDialog *setupDialog;
};

}

#endif
