/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef EBCAMERAVIEW_H
#define EBCAMERAVIEW_H

#include <QLabel>

namespace eb
{

class CameraThread;

class CameraView : public QLabel
{
    Q_OBJECT

public:
    CameraView(CameraThread *camthread, QWidget *parent = 0);
    ~CameraView();

public slots:
    void hide();
    void show();

    void setImage(const QImage &img);

private:
    CameraThread *camThread;
    bool sized;

};

}

#endif // EBCAMERAVIEW_H
