/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef EBSETUPDIALOG_H
#define EBSETUPDIALOG_H

#include <QDialog>

class QSlider;
class QComboBox;
class QLabel;
class QPushButton;

namespace eb
{

class CameraThread;
class CameraView;

class SetupDialog : public QDialog
{
    Q_OBJECT

public:
    SetupDialog(CameraThread *camthread);
    ~SetupDialog();

public slots:
    void accept();
    void reject();
    void refreshCameras();
    void updateFPS(int);
    void updateThreshold(int);

signals:
    void settingsChanged(int threshold, int cameraIdx);

protected:
    void showEvent(QShowEvent *event);
    void hideEvent(QHideEvent *event);

private:
    CameraThread *camThread;
    CameraView *camView;

    QComboBox *camera;
    QSlider *threshold;
    QLabel *thresholdNum;
    QLabel *FPSNum;
    QPushButton *toggleThreshold;

    int oldCamera, oldThreshold;
};

}

#endif // EBSETUPDIALOG_H
