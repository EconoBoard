/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef EBCAMERATHREAD_H
#define EBCAMERATHREAD_H

#include <QThread>
#include <QMutex>
#include <QTimer>
#include <QImage>
#include <cv.h>
#include <highgui.h>

#define SMALLEST_REGION 10

namespace eb
{

class CameraThread : public QThread
{
    Q_OBJECT

public:
    CameraThread(QObject *parent = 0);
    ~CameraThread();

    inline bool canCapture()
    {
        return capture;
    }
    inline int camera()
    {
        return camid;
    }
    inline int threshold()
    {
        return thresh;
    }

public slots:
    void attachView();
    void detachView();

    void grab();

    void setThreshold(int t);
    void setCamera(int idx);
    void setDisplayUpdate(int ms); // set to 0 to stop converting
    void setCaptureUpdate(int ms);
	void setApplyThreshold(bool apply);

private slots:
    void updateFrame();

signals:
    void imageGrabbed(const QImage &img);
    void FPSCalculated(int fps);

protected:
    void run();

private:
    class TargetPoint
    {
    public:
        TargetPoint() : valid(false) {}
        TargetPoint(int X, int Y) : valid(true), x(X), y(Y) {}

        bool valid;
        int x, y;
    };

    TargetPoint tpoint;

    IplImage *captureFrame();

    QImage defaultImage();
    QImage convertImage();

    void allocateBuffers();
    void destroyBuffers();

    TargetPoint applyFilters(IplImage *img);

    int camid, numchannels, thresh;
    int updateDelay, captureDelay, views;
	bool applyThreshold;

    QSize frameSize;

    QTimer updateTimer, captureTimer;
    QMutex filterMut, captureMut;
    QImage tmp;

    IplImage *bufferImg, *filterImg, *tmpImg;
    CvCapture *capture;

};

}

#endif
