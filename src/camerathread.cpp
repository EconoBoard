/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "camerathread.h"

#include <QMutexLocker>
#include <QTime>
#include <QPainter>

#include <cv.h>
#include <highgui.h>

using namespace eb;

CameraThread::CameraThread(QObject *parent)
        : QThread(parent), thresh(50),
        updateDelay(30), views(0),
        bufferImg(0), filterImg(0), capture(0)
{
    frameSize = QSize(320, 240);

    connect(&updateTimer, SIGNAL(timeout()), this, SLOT(grab()));
    connect(&captureTimer, SIGNAL(timeout()), this, SLOT(updateFrame()));

    numchannels = 1;

    setCaptureUpdate(30);
}

CameraThread::~CameraThread()
{
    if (capture)
        cvReleaseCapture(&capture);
    destroyBuffers();
}

IplImage *CameraThread::captureFrame()
{
    QMutexLocker locked(&captureMut);

    if (!capture) return 0;
    IplImage *img = cvQueryFrame( capture );

    if (!img)
        qWarning("Could not get a frame from camera with ID %d",
                 camid);

    return img;
}

void CameraThread::run()
{
    qDebug("Capture thread begun");

    capture = cvCreateCameraCapture(camid);
    if (!capture) {
        qWarning("Could not open camera with ID %d", camid);
        return;
    }

    IplImage *frame;
    if ( !(frame = captureFrame()) )
        return;

    allocateBuffers();

    tmpImg = cvCreateImage(cvSize(frame->width, frame->height),
                                  IPL_DEPTH_8U, numchannels);

    exec();

    cvReleaseCapture(&capture);
    cvReleaseImage(&tmpImg);

    qDebug("Capture thread finished");
}

void CameraThread::updateFrame()
{
    static QTime time;
    static int frames = 0;

    if (!isRunning()) return;

    if (frames == 0)
        time.restart();

    IplImage *frame;
    if ( !(frame = captureFrame()) )
        return;

    filterMut.lock();
    cvConvertImage(frame, tmpImg);
    cvResize(tmpImg, filterImg, CV_INTER_LINEAR);
    tpoint = applyFilters(filterImg);
    filterMut.unlock();

    frames++;
    if (time.elapsed() > 3000) {
        int fps = 1000 * ((float)frames/time.elapsed());
        emit FPSCalculated(fps);
        frames = 0;
    }
}

void CameraThread::setCamera(int idx)
{
    if (idx == camid) return;

    int wasrunning = isRunning();
    if (wasrunning) {
        quit();
        wait();
    }

    camid = idx;

    if (wasrunning)
        start();
}

void CameraThread::attachView()
{
    views++;
    if (!updateTimer.isActive())
        updateTimer.start(updateDelay);
}

void CameraThread::detachView()
{
    views--;
    if (!views)
        updateTimer.stop();
}

void CameraThread::setThreshold(int t)
{
    QMutexLocker locked(&filterMut);
    thresh = t;
}

void CameraThread::setDisplayUpdate(int ms)
{
    updateDelay = ms;
    if (ms == 0) {
        updateTimer.stop();
    }
    else {
        updateTimer.stop();
        updateTimer.start(ms);
    }
}

void CameraThread::setCaptureUpdate(int ms)
{
    captureDelay = ms;
    if (ms == 0) {
        captureTimer.stop();
    }
    else {
        captureTimer.stop();
        captureTimer.start(ms);
    }
}

void CameraThread::grab()
{
    emit imageGrabbed(convertImage());
}

struct pixG
{
    unsigned char v;
};

QImage CameraThread::defaultImage()
{
    QImage img(frameSize.width(), frameSize.height(),
               QImage::Format_RGB32);
    QPainter p(&img);

    p.fillRect(QRect(QPoint(), frameSize), Qt::black);

    p.setPen(QPen(Qt::white));
    p.drawText(QRect(QPoint(), frameSize), Qt::AlignCenter,
               tr("No Camera Selected"));

    return img;
}

QImage CameraThread::convertImage()
{
    QMutexLocker locked(&filterMut);

    if (!bufferImg || !capture)
        return defaultImage();

    QSize size(bufferImg->width, bufferImg->height);
    QImage img(size, QImage::Format_RGB32);

    unsigned char *pixels = (unsigned char*)bufferImg->imageData;

    int offset = 0;
    for (int i=0; i<size.height(); i++) {
        QRgb *line = (QRgb*)img.scanLine(i);
        unsigned char *p = pixels + offset;

        for (int j=0; j<size.width(); j++) {
            line[j] = qRgb(*p, *p, *p);
            p++;
        }

        offset += bufferImg->widthStep;
    }

    QPainter p(&img);
    p.setPen(QPen(QColor(255,0,0)));

    if (tpoint.valid) {
        p.drawLine(QPoint(tpoint.x, 0), QPoint(tpoint.x, img.height()));
        p.drawLine(QPoint(0, tpoint.y), QPoint(img.width(), tpoint.y));
    }
    else {
        p.drawText(QPoint(0,0), tr("Point Lost"));
    }

    tmp = img;

    return tmp;
}

void CameraThread::allocateBuffers()
{
    destroyBuffers();

    QMutexLocker locked(&filterMut);
    bufferImg = cvCreateImage(cvSize(frameSize.width(), frameSize.height()),
                           IPL_DEPTH_8U, numchannels);
    filterImg = cvCreateImage(cvSize(frameSize.width(), frameSize.height()),
                              IPL_DEPTH_8U, numchannels);
}

void CameraThread::destroyBuffers()
{
    QMutexLocker locked(&filterMut);

    if (bufferImg)
        cvReleaseImage(&bufferImg);
    if (filterImg)
        cvReleaseImage(&filterImg);
}

void fill(QRect &r, IplImage *img, unsigned char *row, int x, int y)
{
    row[x] = 254;

    if (x > 0 && row[x-1] == 255) {
        if (x < r.left()) r.setLeft(x);
        fill(r, img, row, x-1, y);
    }
    if (x < img->width-1 && row[x+1] == 255) {
        if (x > r.right()) r.setRight(x);
        fill(r, img, row, x+1, y);
    }
    if (y > 0 && (row-img->widthStep)[x] == 255) {
        if (y < r.top()) r.setTop(y);
        fill(r, img, row-img->widthStep, x, y-1);
    }
    if (y < img->height-1 && (row+img->widthStep)[x] == 255) {
        if (y > r.bottom()) r.setBottom(y);
        fill(r, img, row+img->widthStep, x, y+1);
    }
}

void CameraThread::setApplyThreshold(bool apply)
{
    applyThreshold=apply;
}

CameraThread::TargetPoint CameraThread::applyFilters(IplImage *img)
{   
    if(applyThreshold)
    {
        //are we going to use AdaptiveThreshold?
        cvThreshold(img, bufferImg, (double)thresh/100 * 255, 255, CV_THRESH_BINARY);
    }
    else
    {
        cvCopy(img, bufferImg,NULL);
    }

    QList<QRect> rects;

    unsigned char *pixels = (unsigned char*)bufferImg->imageData;

    int offset = 0;
    for (int i=0; i<bufferImg->height; i++) {
        unsigned char *p = pixels + offset;

        for (int j=0; j<bufferImg->width; j++) {
            if (*p == 255) {
                QRect r(j, i, 1, 1);
                fill(r, bufferImg, pixels+offset, j, i);
                if (r.width() >= SMALLEST_REGION && r.height() >= SMALLEST_REGION)
                  rects.push_back(r);
            }
            p++;
        }

        offset += bufferImg->widthStep;
    }

    float size = 0;
    QRect largest;

    while (!rects.isEmpty()) {
        QRect r = rects.takeLast();
        float s = (r.width() + r.height()) * 0.5;
        if (s > size) {
            size = s;
            largest = r;
        }
    }

    QPoint center = largest.center();

    if (size > 0)
        return TargetPoint(center.x(), center.y());
    else
        return TargetPoint();
}
