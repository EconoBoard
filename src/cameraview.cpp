/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "cameraview.h"
#include "camerathread.h"

#include <QPixmap>
#include <iostream>

using namespace eb;

CameraView::CameraView(CameraThread *camthread, QWidget *parent): QLabel(parent), camThread(camthread), sized(false)
{
    setAlignment(Qt::AlignCenter);
}

CameraView::~CameraView()
{
}

void CameraView::setImage(const QImage &img)
{
    setPixmap(QPixmap::fromImage(img));

    if (!sized)
    {
        resize(img.size());
        sized = true;
    }
}

void CameraView::hide()
{
    disconnect(camThread, SIGNAL(imageGrabbed(QImage)),
               this, SLOT(setImage(QImage)));

    QLabel::hide();
}

void CameraView::show()
{
    connect(camThread, SIGNAL(imageGrabbed(QImage)),
            this, SLOT(setImage(QImage)));

    QLabel::show();
}
