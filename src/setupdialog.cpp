/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "setupdialog.h"
#include "camerathread.h"
#include "cameraview.h"

#include <cv.h>
#include <iostream>

#include <QGridLayout>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLabel>
#include <QComboBox>
#include <QSlider>

using namespace eb;

SetupDialog::SetupDialog(CameraThread *camthread)
        : camThread(camthread)
{
    camView = new CameraView(camThread, this);

    camView->show();

    FPSNum = new QLabel(this);
    toggleThreshold = new QPushButton("Threshold: ");
    toggleThreshold->setCheckable(true);
    connect(toggleThreshold,SIGNAL(toggled(bool)),camThread,SLOT(setApplyThreshold(bool)));

    threshold = new QSlider(Qt::Horizontal);
    connect(threshold, SIGNAL(valueChanged(int)), this, SLOT(updateThreshold(int)));
    connect(threshold, SIGNAL(valueChanged(int)), camThread, SLOT(setThreshold(int)));

    camera = new QComboBox(this);
    connect(camera, SIGNAL(activated(int)), camThread, SLOT(setCamera(int)));

    QDialogButtonBox *buttonBox =
        new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                             Qt::Horizontal, this);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    QPushButton *refresh = new QPushButton(tr("Refresh"), this);

    connect(refresh, SIGNAL(clicked()), this, SLOT(refreshCameras()));

    refreshCameras();
    int row = 0;

    QGridLayout *layout = new QGridLayout;
    layout->setSizeConstraint(QLayout::SetMinimumSize);
    layout->setColumnStretch(2, 2);

    layout->addWidget(new QLabel(tr("Capture From:"), this), row, 0);
    layout->addWidget(camera, row, 1, 1, 2);
 	layout->addWidget(refresh, row++, 3);

    layout->addWidget(camView, row++, 0, 1, 4);

    layout->addWidget(toggleThreshold, row, 0);
    layout->addWidget(threshold, row++, 1, 1, 3);

  	layout->addWidget(FPSNum,row++,0);

    layout->addWidget(buttonBox, row++, 1, 1, 3);


    setLayout(layout);

    setWindowTitle(tr("EconoBoard Setup"));

    threshold->setValue(50);
    toggleThreshold->setChecked(true);

 }

SetupDialog::~SetupDialog()
{
}

void SetupDialog::showEvent(QShowEvent *event)
{
    oldCamera = camThread->camera();
    camera->setCurrentIndex(oldCamera);
    oldThreshold = camThread->threshold();
    threshold->setValue(oldThreshold);

    camThread->attachView();
}

void SetupDialog::hideEvent(QHideEvent *event)
{
    camThread->detachView();
}

void SetupDialog::accept()
{
    qDebug("Settings accepted");
    emit settingsChanged(camThread->threshold(), camThread->camera());

    QDialog::accept();
}

void SetupDialog::reject()
{
    qDebug("Settings rejected");
    camThread->setThreshold(oldThreshold);
    camThread->setCamera(oldCamera);

    QDialog::reject();
}

void SetupDialog::refreshCameras()
{
    int cam = camera->currentIndex();
    camera->clear();

    bool wasrunning = camThread->isRunning();

    if (wasrunning) {
        camThread->quit();
        camThread->wait();
    }

    int numcams = 0;
    CvCapture *cap = cvCreateCameraCapture(numcams);
    while (cap)
    {
        QString str = tr("Camera %1").arg(numcams);
        camera->addItem(str);
        cvReleaseCapture(&cap);
        cap = cvCreateCameraCapture(++numcams);
    }
    cvReleaseCapture(&cap);

    qDebug("Restoring camera id to %d", cam);
    camera->setCurrentIndex(cam);
    camThread->setCamera(cam);

    if (wasrunning)
        camThread->start();
}


void SetupDialog::updateFPS(int FPS)
{
    FPSNum->setText(tr("FPS: %1").arg(FPS));
}

void SetupDialog::updateThreshold(int threshold)
{    
    toggleThreshold->setText(tr("Threshold: %1").arg(threshold));
}



