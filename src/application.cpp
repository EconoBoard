/*************************************************************************
Copyright 2009, Matthew Thompson, Lee Hicks

This file is part of EconoBoard.

    EconoBoard is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EconoBoard is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EconoBoard.  If not, see <http://www.gnu.org/licenses/>.
*************************************************************************/

#include "application.h"
#include "camerathread.h"
#include "cameraview.h"
#include "setupdialog.h"

#include <QSystemTrayIcon>
#include <QSettings>
#include <QAction>
#include <QMenu>
#include <QPixmap>

using namespace eb;

Application::Application(int &argc, char **argv)
        : QApplication(argc, argv)
{
    setQuitOnLastWindowClosed(false);

    camThread = new CameraThread();

    createWidgets();
    createMenus();
    systemTray->show();

    QCoreApplication::setOrganizationName("EconoSoft");
    QCoreApplication::setApplicationName("EconoBoard");
    restoreSettings();

    camThread->start();
}

Application::~Application()
{
    camThread->quit();
    camThread->wait();

    delete setupDialog;

    delete camThread;
}

void Application::createMenus()
{
    QAction *act;

    trayMenu = new QMenu();
    systemTray->setContextMenu(trayMenu);

    trayMenu->addAction(tr("&Setup"), this, SLOT(setup()));
    trayMenu->addAction(tr("&Quit"), this, SLOT(quit()));
}

void Application::createWidgets()
{
    systemTray = new QSystemTrayIcon(QIcon(":tray-img.png"));

    setupDialog = new SetupDialog(camThread);
    connect(setupDialog, SIGNAL(settingsChanged(int,int)), this, SLOT(saveSetup(int,int)));
}

void Application::setup()
{
    setupDialog->show();
    setupDialog->raise();
    setupDialog->activateWindow();
}

void Application::saveSetup(int threshold, int cameraIdx)
{
    QSettings settings;
    settings.setValue("filter/threshold", threshold);
    settings.setValue("camera/index", cameraIdx);

    qDebug("Settings were saved");
}

void Application::restoreSettings()
{
    QSettings settings;

    camThread->setThreshold(settings.value("filter/threshold").toInt());
    camThread->setCamera(settings.value("camera/index").toInt());
}
